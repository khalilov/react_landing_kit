"use strict";
import React from 'react'

export class LandingHeader extends React.Component {
    render() {
        return (
            <header>
                { this.props.children }
            </header>
        )
    }
}