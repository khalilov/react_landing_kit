"use strict";
import React from 'react'

export class LandingQuote extends React.Component {
    render() {
        return (
            <section>
                { this.props.children }
            </section>
        )
    }
}