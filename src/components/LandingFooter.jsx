"use strict";
import React from 'react'

export class LandingFooter extends React.Component {
    render() {
        return (
            <footer>
                { this.props.children }
            </footer>
        )
    }
}