"use strict";
import React from 'react'

export class LandingSection extends React.Component {
    render() {
        return (
            <section>
                { this.props.children }
            </section>
        )
    }
}