"use strict";
import React from 'react'
import { LandingSection, LandingQuote } from 'components'

export class Contacts extends React.Component {

    render() {
        return (
            <LandingSection>
                <h2>Contacts</h2>
                <LandingQuote>
                    Contacts text
                </LandingQuote>
            </LandingSection>
        )
    }
}