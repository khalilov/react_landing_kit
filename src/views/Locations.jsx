"use strict";

import React from 'react'
import { Map, Marker } from 'yandex-map-react'

export class Locations extends React.Component {

    render() {
        let coords = {
            latitude: 55.754734,
            longitude: 37.583314
        };
        let onAPIAvailable = function () {
            console.log('Yandex API available');
        };
        return (
            <Map onAPIAvailable={ onAPIAvailable } center={ [ coords.latitude, coords.longitude ] } zoom={10} width={ 100 }>
                <Marker lat={ coords.latitude } lon={ coords.longitude } />
            </Map>
        )
    }
}