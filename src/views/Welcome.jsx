"use strict";
import React from 'react'

export class Welcome extends React.Component {

    render() {

        return (
            <section>
                <h1>Welcome</h1>
                <blockquote>Welcome text</blockquote>
            </section>
        )
    }
}