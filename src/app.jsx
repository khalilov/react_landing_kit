"use strict";

import React from 'react'
import {SectionsContainer, Section} from 'react-fullpage'
import {Welcome, Locations, Contacts} from 'views'

import './app.styl';

export class App extends React.Component {

    sections = [
        {
            anchor: 'welcome',
            content: (
                <Welcome />
            )
        },
        {
            anchor: 'locations',
            content: (
                <Locations />
            )
        },
        {
            anchor: 'contacts',
            content: (
                <Contacts />
            )
        }
    ];

    options = () => {
        return {
            navigation: true,
            scrollBar: true,
            className: 'page-container',
            sectionClassName: 'page-section',
            activeClass: 'page-active-section',
            anchors: this.sections.map((section) => {
                return section.anchor
            }),
            arrowNavigation: true
        }
    };

    render() {
        console.log(this.options);

        return (
            <SectionsContainer options={ this.options }>
                {
                    this.sections.map((section, index) => {
                        return (
                            <Section key={ index } id={ section.anchor }>
                                { section.content }
                            </Section>
                        )
                    })
                }
            </SectionsContainer>
        )
    }
}