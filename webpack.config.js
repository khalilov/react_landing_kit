const copyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    entry: {
        client: __dirname + '/src/client.js'
    },
    output: {
        filename: '[name].js',
        library: 'name',
        path: __dirname + '/dist',
        publicPath: '/',
    },
    plugins: [
        new copyWebpackPlugin([
            {
                from: __dirname + '/src/html',
                to: __dirname + '/dist'
            }
        ]),
    ],
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                loaders: [ 'babel-loader?presets[]=es2015&presets[]=stage-0&presets[]=react' ]
            },
            {
                test: /\.styl$/,
                loader: 'style-loader!css-loader!stylus-loader'
            }
        ]
    },
    resolve: {
        extensions: [ '', '.js', '.jsx' ],
        modulesDirectories: [ 'node_modules' ],
        alias: {
            'views': __dirname + '/src/views',
            'routes': __dirname + '/src/routes',
            'components': __dirname + '/src/components'
        }
    },
    resolveLoader: {
        extensions: [ '', '.js' ],
        modulesDirectories: [ 'node_modules' ],
        modulesTemplates: [ '*-loader', '*' ]
    },
    eslint: {
        emitErrors: false,
        failOnHint: false,
        resourcePath: __dirname + '/src'
    },
    devServer: {
        inline: true,
        historyApiFallback: true,
        outputPath: __dirname + '/dist',
        contentBase: __dirname + '/dist',
        publicPath: __dirname + '/dist'
    },
    debug: true,
    device: 'source-map',
    progress: true,
    sourceMap: true,
    watch: true
};


